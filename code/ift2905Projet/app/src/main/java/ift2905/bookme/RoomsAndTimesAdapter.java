package ift2905.bookme;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import core.ift2950.bookme.ErrorType;
import core.ift2950.bookme.Room;
import core.ift2950.bookme.RoomManager;

/**
 * Created by Moncef on 2017-04-05.
 */

public class RoomsAndTimesAdapter extends RecyclerView.Adapter<RoomsAndTimesAdapter.MyViewHolder> {

    private Context context;
    private List<RoomManager> itemList;
    private Boolean status;
    private Room room;


    public RoomsAndTimesAdapter(Context context, List<RoomManager> itemList) {
        this.context = context;
        this.itemList = itemList;

    }

    @Override


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.from(parent.getContext())
                .inflate(R.layout.hours_layout, parent, false);
        final int id = view.getId();
        MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        final RoomManager item = itemList.get(position);


        final int temp = position;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "lien vers selection de l'heure ici"+ itemList.get(position).getRoom().id, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context, RoomActivity.class);
                final DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
                intent.putExtra("id", itemList.get(temp).getRoom().id);//pour RoomActivity
                try {
                    Date dateStr = format.parse(NavDrawer.dateBridge);
                    intent.putExtra("date", format.format(dateStr));
                } catch (ParseException e) {

                }
                context.startActivity(intent);
            }
        });


        //List<RoomManager> rManager = NavDrawer.liste;
        int icon = R.drawable.ic_people_black_24dp_copy;
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append("\n " + item.getRoom().id + "\n" + item.getRoom().capacity).append("  ");
        builder.setSpan(new ImageSpan(context, icon), builder.length() - 1, builder.length(), 0);


        //holder.textView0.setText("\nSalle\n"+item.getRoom().id+"\n \n"+item.getRoom().capacity);


        //Toast.makeText(context, var.getRoom().id , Toast.LENGTH_SHORT).show();

        item.setRoomAvailabilityChangedListener(new RoomManager.IRoomAvailabilityChanged() {
            @Override
            public void onRoomAvailabilityChanged(String hour, boolean isAvailable) {
                if (item.IsAvailable(8)) {
                    holder.textView1.setBackgroundResource(R.color.occupied);
                    holder.textView1.setTextColor(ContextCompat.getColor(context, R.color.grey));

                }
                if (item.IsAvailable(9)) {
                    holder.textView2.setBackgroundResource(R.color.occupied);
                    holder.textView2.setTextColor(ContextCompat.getColor(context, R.color.grey));

                }
                if (item.IsAvailable(10)) {
                    holder.textView3.setBackgroundResource(R.color.occupied);
                    holder.textView3.setTextColor(ContextCompat.getColor(context, R.color.grey));

                }
                if (item.IsAvailable(11)) {
                    holder.textView4.setBackgroundResource(R.color.occupied);
                    holder.textView4.setTextColor(ContextCompat.getColor(context, R.color.grey));

                }
                if (item.IsAvailable(12)) {
                    holder.textView5.setBackgroundResource(R.color.occupied);
                    holder.textView5.setTextColor(ContextCompat.getColor(context, R.color.grey));

                }
                if (item.IsAvailable(13)) {
                    holder.textView6.setBackgroundResource(R.color.occupied);
                    holder.textView6.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(14)) {
                    holder.textView7.setBackgroundResource(R.color.occupied);
                    holder.textView7.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(15)) {
                    holder.textView8.setBackgroundResource(R.color.occupied);
                    holder.textView8.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(16)) {
                    holder.textView9.setBackgroundResource(R.color.occupied);
                    holder.textView9.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(17)) {
                    holder.textView10.setBackgroundResource(R.color.occupied);
                    holder.textView10.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(18)) {
                    holder.textView11.setBackgroundResource(R.color.occupied);
                    holder.textView11.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(19)) {
                    holder.textView12.setBackgroundResource(R.color.occupied);
                    holder.textView12.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(20)) {
                    holder.textView13.setBackgroundResource(R.color.occupied);
                    holder.textView13.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }
                if (item.IsAvailable(21)) {
                    holder.textView14.setBackgroundResource(R.color.occupied);
                    holder.textView14.setTextColor(ContextCompat.getColor(context, R.color.grey));
                }

            }

            @Override
            public void onError(ErrorType errorType) {

            }
        });

        //holder.setIsRecyclable(false);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {

        if (itemList != null) {
            return itemList.size();
        }
        return 0;

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        //public CardView horaireCard;

        public TextView textView1;
        public TextView textView2;
        public TextView textView3;
        public TextView textView4;
        public TextView textView5;
        public TextView textView6;
        public TextView textView7;
        public TextView textView8;
        public TextView textView9;
        public TextView textView10;
        public TextView textView11;
        public TextView textView12;
        public TextView textView13;
        public TextView textView14;


        public MyViewHolder(View itemView) {

            super(itemView);


            //horaireCard=(CardView) itemView.findViewById(R.id.horaireCardView);

            textView1 = (TextView) itemView.findViewById(R.id.textView1);
            textView2 = (TextView) itemView.findViewById(R.id.textView2);
            textView3 = (TextView) itemView.findViewById(R.id.textView3);
            textView4 = (TextView) itemView.findViewById(R.id.textView4);
            textView5 = (TextView) itemView.findViewById(R.id.textView5);
            textView6 = (TextView) itemView.findViewById(R.id.textView6);
            textView7 = (TextView) itemView.findViewById(R.id.textView7);
            textView8 = (TextView) itemView.findViewById(R.id.textView8);
            textView9 = (TextView) itemView.findViewById(R.id.textView9);
            textView10 = (TextView) itemView.findViewById(R.id.textView10);
            textView11 = (TextView) itemView.findViewById(R.id.textView11);
            textView12 = (TextView) itemView.findViewById(R.id.textView12);
            textView13 = (TextView) itemView.findViewById(R.id.textView13);
            textView14 = (TextView) itemView.findViewById(R.id.textView14);

        }


    }

}
