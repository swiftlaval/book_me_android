package ift2905.bookme;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import core.ift2950.bookme.Room;
import core.ift2950.bookme.RoomManager;

/**
 * Created by Moncef on 2017-04-05.
 */

public class RoomsAdapter extends RecyclerView.Adapter<RoomsAdapter.MyViewHolder> {

    private Context context;
    private List<RoomManager> itemList;
    private Boolean status;
    private Room room;


    public RoomsAdapter(Context context, List<RoomManager> itemList) {
        this.context = context;
        this.itemList = itemList;

    }

    @Override


    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.from(parent.getContext())
                .inflate(R.layout.liste_salles, parent, false);
        final int id = view.getId();
        MyViewHolder myViewHolder = new MyViewHolder(view);


        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        final RoomManager item = itemList.get(position);


        final int temp = position;

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "lien vers selection de l'heure ici"+ itemList.get(position).getRoom().id, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(context, RoomActivity.class);
                final DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
                intent.putExtra("id", itemList.get(temp).getRoom().id);//pour RoomActivity
                try {
                    Date dateStr = format.parse(NavDrawer.dateBridge);
                    intent.putExtra("date", format.format(dateStr));
                } catch (ParseException e) {

                }
                context.startActivity(intent);
            }
        });


        //List<RoomManager> rManager = NavDrawer.liste;
        int icon = R.drawable.ic_people_black_24dp_copy;
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append("\n " + item.getRoom().id + "\n" + item.getRoom().capacity).append("  ");
        builder.setSpan(new ImageSpan(context, icon), builder.length() - 1, builder.length(), 0);


        //holder.textView0.setText("\nSalle\n"+item.getRoom().id+"\n \n"+item.getRoom().capacity);
        holder.textView0.setText(builder);

        //Toast.makeText(context, var.getRoom().id , Toast.LENGTH_SHORT).show();

        //holder.setIsRecyclable(false);


    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {

        if (itemList != null) {
            return itemList.size();
        }
        return 0;

    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        //public CardView horaireCard;
        public TextView textView0;



        public MyViewHolder(View itemView) {

            super(itemView);


            //horaireCard=(CardView) itemView.findViewById(R.id.horaireCardView);
            textView0 = (TextView) itemView.findViewById(R.id.textView0);


        }


    }

}
