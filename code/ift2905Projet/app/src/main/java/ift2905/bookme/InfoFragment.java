package ift2905.bookme;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import core.ift2950.bookme.RoomManager;
import core.ift2950.bookme.RoomsSearchEngine;

/**
 * Created by Lygia on 10/04/2017.
 */

public class InfoFragment extends Fragment {

    RoomManager manager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_info, container, false);
        Intent intent = getActivity().getIntent();

        final String roomId = intent.getExtras().getString("id");

        DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse("April 25, 2017");
        } catch (ParseException e) {
        }
        RoomsSearchEngine engine = RoomsSearchEngine.getInstance();

        manager = engine.getRoomManager(roomId);

        int capacity =manager.getRoom().capacity;
        boolean hasBlackboard = manager.getRoom().hasBlackboard;
        boolean hasPrinter = manager.getRoom().hasPrinter;
        boolean hasScreen = manager.getRoom().hasScreen;
        String roomImage = manager.getRoom().imageUrl;

        TextView tvRoomId = (TextView) v.findViewById(R.id.roomId);
        tvRoomId.setText(getResources().getString(R.string.room) + " " + roomId);

        TextView tvCapacity = (TextView) v.findViewById(R.id.capacity);
        tvCapacity.setText(getResources().getString(R.string.People));

        TextView nb_people = (TextView) v.findViewById(R.id.nb_people);
        nb_people.setText("" + capacity);


        TextView tvBlackboard = (TextView) v.findViewById(R.id.blackboard);
        tvBlackboard.setText(getResources().getString(R.string.Blackboard));

        ImageView img_hasBlackboard = (ImageView)v.findViewById(R.id.img_hasBlackboard);
        if (hasBlackboard)
            img_hasBlackboard.setImageResource(R.drawable.ic_check_green);
        else
            img_hasBlackboard.setImageResource(R.drawable.ic_notavailable_red);

        TextView tvPrinter = (TextView) v.findViewById(R.id.printer);
        tvPrinter.setText(getResources().getString(R.string.Printer));

        ImageView img_hasPrinter = (ImageView)v.findViewById(R.id.img_hasPrinter);
        if (hasPrinter)
            img_hasPrinter.setImageResource(R.drawable.ic_check_green);
        else
            img_hasPrinter.setImageResource(R.drawable.ic_notavailable_red);

        TextView tvScreen = (TextView) v.findViewById(R.id.screen);
        tvScreen.setText(getResources().getString(R.string.Screen));

        ImageView img_hasScreen = (ImageView)v.findViewById(R.id.img_hasScreen);
        if (hasScreen)
            img_hasScreen.setImageResource(R.drawable.ic_check_green);
        else
            img_hasScreen.setImageResource(R.drawable.ic_notavailable_red);

        ImageView ivRoomImage = (ImageView) v.findViewById(R.id.imageRoom);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity()
                .getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        int height = (int) 0.75 * width;

        Picasso.with(getActivity())
                .load(roomImage)
                .resize(width, height)
                .into(ivRoomImage);


        return v;
    }
}
