package ift2905.bookme;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import core.ift2950.bookme.ErrorType;
import core.ift2950.bookme.RoomManager;
import core.ift2950.bookme.RoomsSearchEngine;

import static ift2905.bookme.NavDrawer.dateBridge;


/**
 * A simple {@link Fragment} subclass.
 */
public class RoomsAndTimesFragment extends Fragment {
    private static final String TAG = "Recycler";
    RoomsAndTimesAdapter adapter;
    RoomsAdapter adapter1;
    private RoomsSearchEngine engine;
    LinearLayoutManager layoutManager;
    LinearLayoutManager layoutManager1;
    RecyclerView horaireRecycler;
    RecyclerView sallesRecycler;


    //CardView horaireCard;
    public RoomsAndTimesFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);//ne pas effacer
        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_rooms_and_times, container, false);
        rootView.setTag(TAG);
        horaireRecycler = (RecyclerView) rootView.findViewById(R.id.horaireRecycler);
        sallesRecycler = (RecyclerView) rootView.findViewById(R.id.sallesRecycler);

        horaireRecycler.setHasFixedSize(true);
        sallesRecycler.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        layoutManager1 = new LinearLayoutManager(getActivity());

        horaireRecycler.setLayoutManager(layoutManager);
        sallesRecycler.setLayoutManager(layoutManager1);

        engine = RoomsSearchEngine.getInstance();
        //solution pour sync de recycler de: http://stackoverflow.com/questions/30702726/sync-scrolling-of-multiple-recyclerviews
        final RecyclerView.OnScrollListener[] scrollListeners = new RecyclerView.OnScrollListener[2];
        scrollListeners[0] = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                horaireRecycler.removeOnScrollListener(scrollListeners[1]);
                horaireRecycler.scrollBy(dx, dy);
                horaireRecycler.addOnScrollListener(scrollListeners[1]);
            }
        };
        scrollListeners[1] = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                sallesRecycler.removeOnScrollListener(scrollListeners[0]);
                sallesRecycler.scrollBy(dx, dy);
                sallesRecycler.addOnScrollListener(scrollListeners[0]);
            }
        };
        sallesRecycler.addOnScrollListener(scrollListeners[0]);
        horaireRecycler.addOnScrollListener(scrollListeners[1]);
//...

        ArrayList<Integer> filter = NavDrawer.getmUserItems();

        int places = -1;
        int places2 = 0;
        int places3 = 0;
        int places8 = 0;
        int bboard = 1;
        int wprinter = 1;
        int screen = 1;

        if (filter.contains(0)) {
            places2 = -1;
        }
        if (filter.contains(1)) {
            places3 = -1;
        }
        if (filter.contains(2)) {
            places8 = -1;
        }
        if (!filter.contains(0) && !filter.contains(1) && !filter.contains(2)) {
            places2 = -1;
            places3 = -1;
            places8 = -1;
        }
        if (filter.contains(3)) {
            bboard = 1;
            wprinter = -1;
            screen = -1;
            if (filter.contains(4)) {
                bboard = 1;
                wprinter = 1;
                screen = -1;
            }
        }
        if (filter.contains(3)) {
            bboard = 1;
            wprinter = -1;
            screen = -1;
            if (filter.contains(5)) {
                bboard = 1;
                wprinter = -1;
                screen = 1;
            }
        }

        if (filter.contains(4)) {

            bboard = -1;
            wprinter = 1;
            screen = -1;
            if (filter.contains(5)) {
                bboard = -1;
                wprinter = 1;
                screen = 1;
            }

        }
        if (filter.contains(4)) {

            bboard = -1;
            wprinter = 1;
            screen = -1;
            if (filter.contains(3)) {
                bboard = 1;
                wprinter = -1;
                screen = 1;
            }

        }
        if (filter.contains(5)) {
            bboard = -1;
            wprinter = -1;
            screen = 1;
            if (filter.contains(4)) {
                bboard = -1;
                wprinter = 1;
                screen = 1;
            }
        }
        if (filter.contains(5)) {
            bboard = -1;
            wprinter = -1;
            screen = 1;
            if (filter.contains(3)) {
                bboard = 1;
                wprinter = -1;
                screen = 1;
            }
        }
        if (!filter.contains(3) && !filter.contains(4) && !filter.contains(5)) {
            bboard = -1;
            wprinter = -1;
            screen = -1;
        }
        if (filter.contains(3) && filter.contains(4) && filter.contains(5)) {
            bboard = 1;
            wprinter = 1;
            screen = 1;
        }


        Log.d(TAG, "onCreateView: " + places2 + " " + places3 + " " + places8 + " " + bboard + " " + wprinter + " " + screen);
        engine.setFilters(places, places2, places3, places8, bboard, wprinter, screen);


        //....

        return rootView;

    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //pour cacher les bouttons des fragments
        getActivity().setTitle(dateBridge);

    }

    @Override
    public void onResume() {
        super.onResume();
        // TODO: changer format date selon langue du telephone
        DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
        Date date1 = null;
        try {
            date1 = format.parse(dateBridge);

        } catch (ParseException e) {

        }


        engine.getRooms(date1, new RoomsSearchEngine.IFoundRoomsListener() {
            @Override
            public void onRoomsFound(final List<RoomManager> roomManagers) {
                if (getActivity() != null) {
                    //horaireCard=(CardView) getView().findViewById(R.id.horaireCardView);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter = new RoomsAndTimesAdapter(getActivity(), roomManagers);

                            horaireRecycler.setAdapter(adapter);

                            adapter1 = new RoomsAdapter(getActivity(), roomManagers);
                            sallesRecycler.setAdapter(adapter1);

                        }
                    });
                }


            }

            @Override
            public void onError(ErrorType errorType) {

            }


        });

    }
}
