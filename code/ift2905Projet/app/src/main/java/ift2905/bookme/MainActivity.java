package ift2905.bookme;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class MainActivity extends AppCompatActivity {

    private Button buttonSignIn;
    private EditText editTextMail;
    private EditText editTextPassword;

    private ProgressDialog progressDialog;

    private FirebaseAuth firebaseAuth;


    NetworkStateReceiver networkStateReceiver = null;

    //As we register/unregister for connectivity events on activity start and stop, this flag
    //helps to know if app need to register again
    Boolean needToRegister = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EditText txt_email = (EditText)findViewById(R.id.editText_email);
        txt_email.setHint(getResources().getString(R.string.emailHint));

        EditText txt_password = (EditText)findViewById(R.id.editText_password);
        txt_password.setHint(getResources().getString(R.string.passwordHint));

        Button bt_connect = (Button)findViewById(R.id.button_signIn);
        bt_connect.setText(getResources().getString(R.string.logIn));

        //Check whether the device is connected to internet or not
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        needToRegister = false;
        firebaseAuth = FirebaseAuth.getInstance();

        progressDialog = new ProgressDialog(this);
        if(isConnected && firebaseAuth.getCurrentUser() != null){
            //User is logged in, go to rooms availability view
            finish();
            startActivity(new Intent(getApplicationContext(), NavDrawer.class));
        } else {
            networkStateReceiver = new NetworkStateReceiver();
            networkStateReceiver.addListener(new NetworkStateReceiver.NetworkStateReceiverListener() {
                @Override
                public void networkAvailable() {
                    progressDialog.dismiss();

                    firebaseAuth = FirebaseAuth.getInstance();

                    //check if user is logged in
                    if(firebaseAuth.getCurrentUser() != null) {
                        finish();
                        startActivity(new Intent(getApplicationContext(), NavDrawer.class));
                    }
                }

                @Override
                public void networkUnavailable() {
                    progressDialog.setTitle(R.string.noInternetTitle);
                    progressDialog.setMessage(getResources().getString(R.string.noInternetMessage));
                    progressDialog.show();
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                }
            });

            this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }


        buttonSignIn = (Button)findViewById(R.id.button_signIn);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLogin();
            }
        });

        editTextMail = (EditText)findViewById(R.id.editText_email);

        editTextPassword = (EditText)findViewById(R.id.editText_password);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if(needToRegister){
            needToRegister = false;
            this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    protected void onStop() {
        needToRegister = true;
        this.unregisterReceiver(networkStateReceiver);
        super.onStop();
    }

    private void userLogin() {

        String email = editTextMail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            //email is empty
            Toast.makeText(getApplicationContext(), R.string.noEmail, Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            //password is empty
            Toast.makeText(getApplicationContext(), R.string.noPassword, Toast.LENGTH_SHORT).show();
            return;
        }

        //if validations are ok
        //we will first show a progress dialog
        progressDialog.setMessage(getResources().getString(R.string.loggingIn));
        progressDialog.show();

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressDialog.dismiss();
                        if (task.isSuccessful()) {
                            //start the room availabity activity
                            finish();
                            startActivity(new Intent(getApplicationContext(), NavDrawer.class));
                        }
                        else {
                            Toast.makeText(getApplicationContext(), R.string.wrongCredentials, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
