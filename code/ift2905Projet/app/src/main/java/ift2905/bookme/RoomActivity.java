package ift2905.bookme;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.IntentFilter;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import core.ift2950.bookme.ReservationsManager;
import core.ift2950.bookme.SettingsManager;

public class RoomActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager pager;
    RoomPagerAdapter pagerAdapter;
    String roomId;
    Date date;
    String dateString;
    static Calendar calendar;
    DateFormat formatter = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
    Fragment scheduleFragment;

    private String[] titles = {"", ""};
    private int[] icons = {R.drawable.ic_agenda, R.drawable.ic_info};

    NetworkStateReceiver networkStateReceiver = null;
    Boolean needToRegister = true;
    Boolean syncUserInformation = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ProgressDialog progressDialog = new ProgressDialog(this);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(new NetworkStateReceiver.NetworkStateReceiverListener() {
            @Override
            public void networkAvailable() {
                progressDialog.dismiss();

                if(!syncUserInformation) {
                    syncUserInformation = true;
                    ReservationsManager.getInstance().syncUserReservation
                            (FirebaseAuth.getInstance().getCurrentUser().getEmail(), null);
                    SettingsManager.getInstance().syncUserSettings
                            (FirebaseAuth.getInstance().getCurrentUser().getEmail(), null);
                }
            }

            @Override
            public void networkUnavailable() {
                progressDialog.setTitle(R.string.noInternetTitle);
                progressDialog.setMessage(getResources().getString(R.string.noInternetMessage));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            }
        });

        this.needToRegister = false;
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        setContentView(R.layout.activity_room);

        tabLayout = (TabLayout)findViewById(R.id.tabLayout);
        pager = (ViewPager)findViewById(R.id.pager_room);

        calendar = NavDrawer.dateTime;

        roomId = getIntent().getExtras().getString("id");
        String tab = "" + getIntent().getExtras().getString("tab", "schedule");

        titles[0] = getResources().getString(R.string.reserve_tab_title);
        titles[1] = getResources().getString(R.string.room) + " " + roomId;

        date = calendar.getTime();
        dateString = formatter.format(date);

        setTitle(dateString);

        pagerAdapter = new RoomPagerAdapter(getSupportFragmentManager(), tab);

        pager.setAdapter(pagerAdapter);
        if (tab.equalsIgnoreCase("schedule"))
            selectPage(0);
        else
            selectPage(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_room, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.calendar_room)
            updateDate();

        return super.onOptionsItemSelected(item);
    }

    /**
     * adapter for tabLayout
     */
    public class RoomPagerAdapter extends FragmentPagerAdapter {

        String tab;

        public RoomPagerAdapter(FragmentManager fm, String tab) {
            super(fm);
            this.tab = tab;
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0)
                return scheduleFragment = new ScheduleFragment();
            else
                return new InfoFragment();
        }

        @Override
        public int getCount() {
            return titles.length;
        }

        @Override
        public CharSequence getPageTitle(int position){
            return titles[position];
        }


    }

    /**
     * selection of tab
     */
    void selectPage(int pageIndex)
    {
        pager.setCurrentItem(pageIndex);
        tabLayout.setupWithViewPager(pager);

        tabLayout.getTabAt(0).setIcon(icons[0]).setText(titles[0]);
        tabLayout.getTabAt(1).setIcon(icons[1]).setText(titles[1]);
    }

    /**
     * show datePicker and update selected date
     */
    private void updateDate(){
        DatePickerDialog pickerDialog = new DatePickerDialog (this, d, calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        pickerDialog.show();
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            date = calendar.getTime();
            dateString = formatter.format(date);
            setTitle(dateString);

            getSupportFragmentManager()
                    .beginTransaction()
                    .detach(scheduleFragment)
                    .commitNowAllowingStateLoss();

            getSupportFragmentManager()
                    .beginTransaction()
                    .attach(scheduleFragment)
                    .commitAllowingStateLoss();
        }
    };
}