package ift2905.bookme;

import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import core.ift2950.bookme.Reservation;

/**
 * Created by ekabore on 17-04-20.
 */
public class ReservationRecyclerViewAdapter extends RecyclerView.Adapter<ReservationRecyclerViewAdapter.ReservationViewHolder> {

    private List<Reservation> reservations;
    private IOnReservationMoreOptionRequested reservationMoreOptionRequested;

    public interface IOnReservationMoreOptionRequested{
        public void onMoreOptionRequested(Reservation reservation);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ReservationViewHolder extends RecyclerView.ViewHolder {
        TextView txtReservationRoomId;
        TextView txtReservationDate;
        TextView txtReservationHour;
        TextView txtReservationDescription;

        ImageButton btnReservationMoreOptions;
        View reservationDateIndication;

        public ReservationViewHolder(View itemView) {
            super(itemView);

            txtReservationRoomId = (TextView)itemView.findViewById(R.id.roomId);
            txtReservationDate = (TextView)itemView.findViewById(R.id.txtReservationDate);
            txtReservationHour = (TextView) itemView.findViewById(R.id.txtReservartionHour);
            txtReservationDescription = (TextView)itemView.findViewById(R.id.txtReservationDescription);

            btnReservationMoreOptions = (ImageButton) itemView.findViewById(R.id.btnReservationMoreOptions);
            reservationDateIndication = (View) itemView.findViewById(R.id.reservationDateIndication);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReservationRecyclerViewAdapter(IOnReservationMoreOptionRequested reservationMoreOptionRequested) {
        this.reservations = new ArrayList<Reservation>();
        this.reservationMoreOptionRequested = reservationMoreOptionRequested;
    }

    public void addReservation(Reservation reservation){
        this.reservations.add(reservation);
        this.notifyItemInserted(this.reservations.size() - 1);
    }

    public void removeReservation(Reservation reservation){
        if(this.reservations.remove(reservation)) {
            this.notifyDataSetChanged();
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ReservationRecyclerViewAdapter.ReservationViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.reservation_layout, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ReservationViewHolder reservationViewHolder = new ReservationViewHolder(v);

        return reservationViewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ReservationViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        final Reservation reservation = this.reservations.get(position);


        holder.txtReservationRoomId.setText(holder.txtReservationRoomId.getContext().getResources().getString(R.string.room) + " " + reservation.room);
        holder.txtReservationDescription.setText(reservation.description == null ? "" : reservation.description);

        Date date = reservation.getDate();
        DateFormat format = android.text.format.DateFormat.getMediumDateFormat(holder.txtReservationDate.getContext());
        holder.txtReservationDate.setText(format.format(date));

        Integer beginHour = Integer.valueOf((String)reservation.timeSlot.keySet().toArray()[0]);
        Integer duration = reservation.timeSlot.get(beginHour.toString());
        holder.txtReservationHour.setText(beginHour + ":00  -  " + (beginHour + duration) + ":00  (" + duration + "h)");


        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date);

        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);

        if(sameDay){
            int color = ContextCompat.getColor(holder.reservationDateIndication.getContext(), android.R.color.holo_orange_light);
            holder.reservationDateIndication.setBackgroundColor(color);
        } else if(date.before(new Date())) {
            holder.reservationDateIndication.setBackgroundColor(Color.parseColor("#7f8c8d"));
        } else {
            holder.reservationDateIndication.setBackgroundColor(Color.parseColor("#ff99cc00"));
        }

        holder.btnReservationMoreOptions.setOnClickListener(null);
        holder.btnReservationMoreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(reservationMoreOptionRequested != null) {
                    reservationMoreOptionRequested.onMoreOptionRequested(reservation);
                }
            }
        });
    }

    // Return the size of the reservations list (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return this.reservations == null ? 0 : this.reservations.size();
    }
}

