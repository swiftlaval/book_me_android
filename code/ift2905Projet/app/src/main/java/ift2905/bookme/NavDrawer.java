package ift2905.bookme;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import core.ift2950.bookme.ReservationsManager;
import core.ift2950.bookme.SettingsManager;

public class NavDrawer extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseAuth firebaseAuth;


    static DateFormat dateTimeF = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
    static Calendar dateTime = Calendar.getInstance();
    static Date date = new Date();
    public static String dateBridge = dateTimeF.format(date);

    String[] listItems;
    boolean[] checkedItems;
    static ArrayList<Integer> mUserItems = new ArrayList<>();


    private TextView textViewUserEmail;


    NetworkStateReceiver networkStateReceiver = null;
    Boolean needToRegister = true;
    Boolean syncUserInformation = false;
    NavigationView navigationView;
    Activity thisActivity = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        thisActivity = this;

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(new NetworkStateReceiver.NetworkStateReceiverListener() {
            @Override
            public void networkAvailable() {
                progressDialog.dismiss();

                if (!syncUserInformation) {
                    syncUserInformation = true;
                    ReservationsManager.getInstance().syncUserReservation(FirebaseAuth.getInstance().getCurrentUser().getEmail(), null);
                    SettingsManager.getInstance().syncUserSettings(FirebaseAuth.getInstance().getCurrentUser().getEmail(), null);
                }
            }

            @Override
            public void networkUnavailable() {
                progressDialog.setTitle(R.string.noInternetTitle);
                progressDialog.setMessage(getResources().getString(R.string.noInternetMessage));
                progressDialog.show();
                progressDialog.setCancelable(false);
                progressDialog.setCanceledOnTouchOutside(false);
            }
        });

        this.needToRegister = false;
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        setContentView(R.layout.activity_nav_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        firebaseAuth = firebaseAuth.getInstance();
        FirebaseUser user = firebaseAuth.getCurrentUser();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        textViewUserEmail = (TextView) header.findViewById(R.id.textViewEmail);
        textViewUserEmail.setText(getResources().getString(R.string.welcome) + "\n " + user.getEmail());


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        RoomsAndTimesFragment rooms = new RoomsAndTimesFragment();
        FragmentManager manager = getSupportFragmentManager();

        manager.beginTransaction().replace(R.id.ConstraintLayout_settings, rooms, rooms.getTag()).commit();


        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.nav_day);
        this.setTitle(dateBridge);
        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.findViewById(R.id.toolbar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mToolbar.getTitle() == dateBridge) {
                    updateDate();
                } else {
                    Log.d("error", "toolbar clicked in wrong place");
                }

            }
        });


        listItems = getResources().getStringArray(R.array.filter_item);
        checkedItems = new boolean[listItems.length];
        /*for(int i=0;i<listItems.length;i++){
            checkedItems[i]=true;
            mUserItems.add(i);
        }*/


    }


    @Override
    protected void onStart() {
        super.onStart();
        if (needToRegister) {
            needToRegister = false;
            this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    @Override
    protected void onStop() {
        needToRegister = true;
        this.unregisterReceiver(networkStateReceiver);
        super.onStop();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_filter) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(NavDrawer.this);
            mBuilder.setTitle(R.string.dialog_title);
            mBuilder.setMultiChoiceItems(listItems, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int position, boolean isChecked) {

                    if (isChecked) {

                        mUserItems.add(position);
                    } else {

                        mUserItems.remove((Integer.valueOf(position)));
                    }
                }
            });

            mBuilder.setCancelable(false);
            mBuilder.setPositiveButton(R.string.ok_label, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {

                    RoomsAndTimesFragment rooms = new RoomsAndTimesFragment();
                    FragmentManager manager = getSupportFragmentManager();


                    manager.beginTransaction().replace(R.id.ConstraintLayout_settings, rooms, rooms.getTag()).commit();

                }
            });

            mBuilder.setNegativeButton(R.string.dismiss_label, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {


                    dialogInterface.dismiss();


                }
            });

            mBuilder.setNeutralButton(R.string.clear_all_label, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    for (int i = 0; i < checkedItems.length; i++) {
                        checkedItems[i] = false;
                        mUserItems.clear();

                    }
                }
            });

            AlertDialog mDialog = mBuilder.create();
            mDialog.show();
        }


        if (id == R.id.action_calendar) {

            updateDate();

        }

        return super.onOptionsItemSelected(item);
    }

    public static ArrayList<Integer> getmUserItems() {

        return mUserItems;
    }


    MesReservationsFragment mesReservationsFrag;
    RoomsAndTimesFragment roomsFrag;
    Settings settingsFrag;


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        firebaseAuth = firebaseAuth.getInstance();
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        showFragment(id);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*
    Show the fragment that belongs to the id
     */
    void showFragment(int id) {
        if (id == R.id.nav_day && roomsFrag == null) {
            roomsFrag = new RoomsAndTimesFragment();
            FragmentManager manager = getSupportFragmentManager();
            //                                    le frag remplacé             le remplacant
            manager.beginTransaction().replace(R.id.ConstraintLayout_settings, roomsFrag, roomsFrag.getTag()).commit();

            if (mesReservationsFrag != null) {
                mesReservationsFrag.setNotificationManagerListener(null);
                mesReservationsFrag = null;
            }

            settingsFrag = null;
        } else if (id == R.id.nav_reservations && mesReservationsFrag == null) {
            mesReservationsFrag = new MesReservationsFragment();
            mesReservationsFrag.setNotificationManagerListener(new MesReservationsFragment.INotificationManagerListener() {
                @Override
                public void onNotificationManagerAsked() {
                    showFragment(R.id.nav_settings);
                    navigationView.setCheckedItem(R.id.nav_settings);

                }

                @Override
                public void onReservationDeleted(String reservationId) {
                    SharedPreferences prefs = getSharedPreferences("NotificationPref", Context.MODE_PRIVATE);
                    int id = prefs.getInt(reservationId, -1);
                    if (id != -1) {
                        Notification notification = getNotification("", "", R.drawable.alarm_clock);
                        removeNotification(notification, id);
                    }
                }
            });

            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.ConstraintLayout_settings, mesReservationsFrag, mesReservationsFrag.getTag()).commit();

            roomsFrag = null;
            settingsFrag = null;
        } else if (id == R.id.nav_settings && settingsFrag == null) {
            //settings fragment:
            settingsFrag = new Settings();
            FragmentManager manager = getSupportFragmentManager();

            manager.beginTransaction().replace(R.id.ConstraintLayout_settings, settingsFrag, settingsFrag.getTag()).commit();

            if (mesReservationsFrag != null) {
                mesReservationsFrag.setNotificationManagerListener(null);
                mesReservationsFrag = null;
            }

            roomsFrag = null;
        } else if (id == R.id.nav_logout) {
            if (mesReservationsFrag != null) {
                mesReservationsFrag.setNotificationManagerListener(null);
                mesReservationsFrag = null;
            }
            firebaseAuth.signOut();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }


    private void updateDate() {
        //permet d'ouvrir le datepicker
        DatePickerDialog pickerDialog = new DatePickerDialog(this, d, dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DAY_OF_MONTH));
        pickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        pickerDialog.show();
    }

    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateTime.set(Calendar.YEAR, year);
            dateTime.set(Calendar.MONTH, monthOfYear);
            dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateView();


        }
    };

    private void updateView() {
        //permet de mettre a jour l'activity
        //Toast.makeText(this, dateTimeF.format(dateTime.getTime()), Toast.LENGTH_LONG).show();
        dateBridge = dateTimeF.format(dateTime.getTime());
        this.setTitle(dateBridge);
        RoomsAndTimesFragment rooms = new RoomsAndTimesFragment();
        FragmentManager manager = getSupportFragmentManager();


        manager.beginTransaction().replace(R.id.ConstraintLayout_settings, rooms, rooms.getTag()).commit();
    }


    private void scheduleNotification(Notification notification, int delay, int id) {
        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_IDENTIFIER, id);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        long futureInMillis = SystemClock.elapsedRealtime() + delay;
        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, futureInMillis, pendingIntent);
    }

    private void removeNotification(Notification notification, int id) {
        Intent notificationIntent = new Intent(this, NotificationPublisher.class);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION_IDENTIFIER, id);
        notificationIntent.putExtra(NotificationPublisher.NOTIFICATION, notification);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }


    private Notification getNotification(String title, String description, int iconId) {
        Notification.Builder builder = new Notification.Builder(this);
        builder.setContentTitle(title);
        builder.setContentText(description);
        builder.setSmallIcon(iconId);
        return builder.build();
    }

}
