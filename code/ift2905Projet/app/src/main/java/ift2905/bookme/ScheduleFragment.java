package ift2905.bookme;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import core.ift2950.bookme.ErrorType;
import core.ift2950.bookme.ReservationsManager;
import core.ift2950.bookme.RoomManager;
import core.ift2950.bookme.RoomsSearchEngine;

/**
 * Created by Lygia on 09/04/2017.
 *
 * Agenda d'une salle.
 */

public class ScheduleFragment extends Fragment {

    Button bt_confirm;
    ListView list;
    ScheduleAdapter adapter;
    ScheduleFetcher fetcher;
    RoomManager manager;
    ReservationsManager resManager;
    String roomId;
    Date date;
    int curr_hour;
    Calendar calendar = RoomActivity.calendar;
    Calendar curr_calendar = Calendar.getInstance();
    String dateString;

    // schedule times
    String[] hours = {"8:00 - 9:00", "9:00 - 10:00", "10:00 - 11:00", "11:00 - 12:00",
            "12:00 - 13:00", "13:00 - 14:00", "14:00 - 15:00", "15:00 - 16:00", "16:00 - 17:00",
            "17:00 - 18:00", "18:00 - 19:00", "19:00 - 20:00", "20:00 - 21:00", "21:00 - 22:00"};

    // true : time is available, false : not available
    boolean[] availability = new boolean[hours.length];
    // items selected by user
    boolean[] checkedItems = new boolean[hours.length];
    // first hour selected
    int begin = 0;
    // number of hours for a reservation
    int duration = 0;

    @Nullable
    @Override
    public View onCreateView
            (LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_schedule, container, false);
        list = (ListView)v.findViewById(R.id.listView_hours);
        bt_confirm = (Button)v.findViewById(R.id.bt_confirm);
        bt_confirm.setText(getResources().getString(R.string.Confirm));
        bt_confirm.setTextColor(Color.WHITE);

        fetcher = new ScheduleFetcher();
        fetcher.execute();

        return v;
    }

    /**
     *
     * @return
     * 0 if selection is good
     * 1 if duration == 0
     * 2 if duration > 3
     * 3 if sparse hours
     */
    private int checkedItemsIsValid(){
        int nbChecked = 0;
        int lastChecked = -1;
        int newChecked = -1;
        for (int i=0; i<checkedItems.length; i++){
            if (checkedItems[i] == true){
                nbChecked++;
                if (nbChecked > 3) {
                    return 2;
                }
                if (lastChecked == -1){
                    lastChecked = i;
                    newChecked = i;
                    continue;
                }
                newChecked = i;
                if (newChecked - lastChecked > 1)
                    return 3;
                lastChecked = i;
            }
        }
        if (nbChecked == 0) return 1;

        updateBeginDuration();
        return 0;
    }

    /**
     * update begin and duration of reservation from checkedItems
     */
    private void updateBeginDuration() {
        begin = 0;
        duration = 0;
        for (int i=0; i<checkedItems.length; i++) {
            if (checkedItems[i] == true) {
                duration++;
                if (begin == 0)
                    begin = i + 8;
            }
        }
        if (bt_confirm != null) {
            if (duration == 0) {
                bt_confirm.setBackgroundResource(R.color.grey_confirm_unavailable);
            }
            else {
                bt_confirm.setBackgroundResource(R.color.red);
            }
        }
    }

    /**
     *
     * @param checkedItems
     * @return true if no hour is checked
     */
    private boolean isEmpty(boolean checkedItems[]) {
        if (duration == 0)
            return true;
        return false;
    }

    /**
     * sets all items of checkedItems[] to false
     */
    private void clearCheckedItems() {
        for (int i=0; i<checkedItems.length; i++)
            checkedItems[i] = false;
        updateBeginDuration();
    }

    /**
     * updates curr_hour
     */
    private void updateHour() {
        curr_hour = calendar.get(Calendar.HOUR_OF_DAY);
    }

    /**
     * Adapter for the ListView Schedule
     */
    private class ScheduleAdapter extends BaseAdapter {

        LayoutInflater inflater;

        public ScheduleAdapter() {

            this.inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return hours.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }


        @Override
        public View getView(int position, final View convertView, final ViewGroup parent) {
            final int pos = position;
            View cv = inflater.inflate(R.layout.row_hour, parent, false);
            final CheckBox checkbox = (CheckBox)cv.findViewById(R.id.hour_checkBox);
            TextView hour_text = (TextView)cv.findViewById(R.id.hour_text);
            final LinearLayout row_background = (LinearLayout)cv.findViewById(R.id.layout_row);
            ImageView clock_row_hour = (ImageView)cv.findViewById(R.id.clock_row_hour);

            updateHour();

            Date selected_date = calendar.getTime();
            Date curr_date = curr_calendar.getTime();

            // if hour not available or past, gray background, no checkbox
            if (!availability[position] ||
                    (selected_date.before(curr_date) &&
                            (position + 8) < curr_calendar.get(Calendar.HOUR_OF_DAY))) {

                checkbox.setVisibility(View.INVISIBLE);
                hour_text.setTextColor(ContextCompat.getColor(getContext(), R.color.grey));
                clock_row_hour.setColorFilter(ContextCompat.getColor(getContext(), R.color.grey));
                row_background.setBackgroundResource(R.color.occupied);
            }
            // if hour available, checkbox visible if time > curr_time
            else {
                row_background.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        checkbox.performClick();
                    }
                });
                checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkbox.isChecked()) {
                            checkedItems[pos] = true;
                            String errorToast = "";
                            int errorType = checkedItemsIsValid();
                            // selection is valid
                            if (errorType == 0) {
                                row_background.setBackgroundResource(R.color.selected);
                            }
                            // if selection not valid, display msg error
                            // function returns type of error
                            else {
                                if (errorType == 2)
                                    errorToast = getResources().getString(R.string.msg_3_hours);
                                if (errorType == 3)
                                    errorToast = getResources().getString(R.string.msg_contiguous_time);
                                checkedItems[pos] = false;
                                Toast.makeText(getContext(),
                                        errorToast, Toast.LENGTH_SHORT).show();
                                updateBeginDuration();
                                checkbox.setChecked(false);
                            }
                        }
                        else {
                            checkedItems[pos] = false;
                            updateBeginDuration();
                            row_background.setBackgroundResource(R.color.cardview_light_background);
                        }
                    }

                });
                // if CheckBox already selected
                if (checkedItems[position] == true) {
                    checkbox.setChecked(true);
                    row_background.setBackgroundResource(R.color.selected);
                }
            }

            TextView tv = (TextView)cv.findViewById(R.id.hour_text);
            tv.setText(hours[position]);

            return cv;
        }
    }

    private class ScheduleFetcher extends AsyncTask <Object, Object, boolean[]> {

        @Override
        protected boolean[] doInBackground(Object... params) {

            for (int i=0; i<availability.length; i++)
                availability[i] = true;

            // no items are checked at first
            clearCheckedItems();

            // get the room number passed from the intent
            Intent intent = getActivity().getIntent();
            roomId = intent.getExtras().getString("id");

            // get date from calendar;
            date = calendar.getTime();

            updateHour();

            DateFormat formatter = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
            dateString = formatter.format(date);

            adapter = new ScheduleAdapter();

            resManager = new ReservationsManager();
            // manager for the room
            manager = RoomsSearchEngine.getInstance().getRoomManager(roomId);

            return availability;
        }

        @Override
        protected void onPostExecute(final boolean[] availability) {

            manager.setAgendaDate(date);

            // refresh if availability for the room changed
            manager.setRoomAvailabilityChangedListener(new RoomManager.IRoomAvailabilityChanged() {
                @Override
                public void onRoomAvailabilityChanged(String hour, boolean isAvailable) {
                    int position = Integer.parseInt(hour) - 8;
                    availability[position] = !isAvailable;
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onError(ErrorType errorType) {
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_communication),
                            Toast.LENGTH_SHORT).show();
                }
            });

            // reserve button only available if hours are selected
            bt_confirm.setOnClickListener(new View.OnClickListener() {
                // to add a reservation
                @Override
                public void onClick(View v) {
                    // alert if no time was selected
                    if (duration == 0)
                        new AlertDialog.Builder(getContext())
                                .setMessage(getResources().getString(R.string.msg_select_hours))
                                .setPositiveButton(android.R.string.yes,
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                // confirm OK
                                            }
                                        })
                                .show();
                        // if OK, ask description, confirm date, time and room
                    else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                        builder.setTitle(getResources().getString(R.string.new_reservation_title));
                        builder.setMessage("" + dateString + "\n" +
                                getResources().getString(R.string.room) + " " +
                                roomId + "\n" +
                                getResources().getString(R.string.From) +
                                begin + ":00 " + getResources().getString(R.string.to) + " " +
                                (begin + duration) + ":00\n" +
                                getResources().getString(R.string.res_description_title));
                        final EditText input = new EditText(getContext());
                        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_LONG_MESSAGE);
                        builder.setView(input);
                        builder.setPositiveButton
                                (android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int which) {
                                        String confirmation = "";
                                        int errorType = checkedItemsIsValid();
                                        // send demand to add reservation
                                        if (errorType == 0) {
                                            String description = input.getText().toString();
                                            confirmation = resManager.addReservation
                                                    (FirebaseAuth.getInstance().getCurrentUser().getEmail(),
                                                            roomId, date, description, begin, duration);
                                            // reservation done, alert confirmation
                                            if (!confirmation.equals("")) {
                                                clearCheckedItems();
                                                new AlertDialog.Builder(getContext())
                                                        .setTitle(getResources().getString(R.string.Confirmation))
                                                        .setMessage("\n" + getResources().getString(R.string.Res_number) +
                                                                ": " + confirmation + "\n" +
                                                                getResources().getString(R.string.msg_late_cancel))
                                                        .setPositiveButton(android.R.string.yes,
                                                                new DialogInterface.OnClickListener() {

                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        // confirm OK
                                                                    }
                                                                })
                                                        .show();
                                            }
                                            // reservation not done
                                            else
                                                new AlertDialog.Builder(getContext())
                                                        .setTitle("Échec")
                                                        .setMessage("\nImpossible de faire votre réservation, veuillez réessayer.")
                                                        .setPositiveButton(android.R.string.yes,
                                                                new DialogInterface.OnClickListener() {

                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        // confirm OK
                                                                    }
                                                                })
                                                        .show();
                                        }
                                    }
                                });
                        builder.setNegativeButton
                                (android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        // do nothing
                                    }
                                })
                                .show();
                }
                }
            });

            list.setAdapter(adapter);
        }


    }

}