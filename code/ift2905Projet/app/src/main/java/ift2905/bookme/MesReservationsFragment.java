package ift2905.bookme;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import core.ift2950.bookme.ErrorType;
import core.ift2950.bookme.Reservation;
import core.ift2950.bookme.ReservationsManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class MesReservationsFragment extends Fragment {

    //Key of the value that indicates if the user has already seen tips and clicked on "understood"
    static String SHOW_TIP_KEY = "reservationTipsAlreadyShown";

    private RecyclerView mRecyclerView;
    private ReservationRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private INotificationManagerListener notificationManagerListener;

    public MesReservationsFragment() {
        // Required empty public constructor
    }

    public void setNotificationManagerListener(INotificationManagerListener notificationManagerListener){
        this.notificationManagerListener = notificationManagerListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        final Fragment myReservationFragment = this;

        final View view = inflater.inflate(R.layout.fragment_mes_reservations, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rcv_reservations);

        final ImageView imgEmptyReservation = (ImageView)view.findViewById(R.id.imgEmptyReservation);
        final TextView txtEmptyReservation = (TextView)view.findViewById(R.id.txtEmptyReservation);

        // specify an adapter (see also next example)
        mAdapter = new ReservationRecyclerViewAdapter(new ReservationRecyclerViewAdapter.IOnReservationMoreOptionRequested() {
            @Override
            public void onMoreOptionRequested(Reservation reservation) {
                onShowPopup(view, reservation);
            }
        });

        ReservationsManager.getInstance().syncUserReservation(FirebaseAuth.getInstance().getCurrentUser().getEmail(), new ReservationsManager.IReservationsListener() {
            @Override
            public void onReservationFound(final Reservation reservation) {
                myReservationFragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imgEmptyReservation.setVisibility(View.GONE);
                        txtEmptyReservation.setVisibility(View.GONE);
                        if(mAdapter != null){
                            mAdapter.addReservation(reservation);
                        }
                    }
                });
            }

            @Override
            public void onReservationDeleted(final Reservation reservation) {
                myReservationFragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(mAdapter != null) {
                            mAdapter.removeReservation(reservation);
                        }
                    }
                });
                if(notificationManagerListener != null){
                    notificationManagerListener.onReservationDeleted(reservation.id);
                }
            }

            @Override
            public void onStartFetchingReservations(String user, final List<Reservation> allReadyFetched) {
                myReservationFragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(allReadyFetched != null && allReadyFetched.size() > 0){
                            //Check if user already understood the tips and if not show tips
                            SharedPreferences prefs = myReservationFragment.getActivity().getPreferences(Context.MODE_PRIVATE);
                            Boolean tipAlreadyUnderstood = prefs.getBoolean(SHOW_TIP_KEY, false);
                            if(!tipAlreadyUnderstood){
                                showTip(prefs);
                            }

                            imgEmptyReservation.setVisibility(View.GONE);
                            txtEmptyReservation.setVisibility(View.GONE);
                            if(mAdapter != null){
                                for(Reservation reservation : allReadyFetched) {
                                    mAdapter.addReservation(reservation);
                                }
                            }
                        } else {
                            txtEmptyReservation.setVisibility(View.GONE);
                        }
                    }
                });
            }

            @Override
            public void onUserReservationEmpty() {
                myReservationFragment.getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imgEmptyReservation.setVisibility(View.VISIBLE);
                        txtEmptyReservation.setVisibility(View.VISIBLE);
                    }
                });
            }

            @Override
            public void onError(ErrorType errorType) {

            }
        });

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(mAdapter);

        // Inflate the layout for this fragment
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        ReservationsManager.getInstance().syncUserReservation(FirebaseAuth.getInstance().getCurrentUser().getEmail(), null);
    }

    Dialog dialog;
    PopupWindow popWindow;
    View reservationGuide = null;

    /**
     * Show tip to user: the tip is about the different colors of reservations card.
     * @param preferences   preferences where is stored the value indicating if the user has already understood the tip
     */
    void showTip(final SharedPreferences preferences){
        if(reservationGuide != null)
            return;

        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // inflate the custom popup layout
        reservationGuide = layoutInflater.inflate(R.layout.reservation_guide, null, false);

        ImageButton btnCancel = (ImageButton)reservationGuide.findViewById(R.id.btnCancel);
        final Button btnUnderstood = (Button)reservationGuide.findViewById(R.id.btnUnderstood);

        dialog = new Dialog(getContext(), android.R.style.Theme_Translucent_NoTitleBar);
        final View emptyDialog = LayoutInflater.from(getContext()).inflate(R.layout.empty, null);

        // set height depends on the device size
        popWindow = new PopupWindow(reservationGuide, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        // make it outside touchable to dismiss the popup window
        popWindow.setOutsideTouchable(true);

         /* blur background*/
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount=0.8f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(emptyDialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnShowListener(new DialogInterface.OnShowListener()
        {
            @Override
            public void onShow(DialogInterface dialogIx)
            {
                // show the popup at bottom of the screen and set some margin at bottom ie,
                popWindow.showAtLocation(emptyDialog, Gravity.BOTTOM, 0, 0);
            }
        });
        popWindow.setOnDismissListener(new PopupWindow.OnDismissListener()
        {
            @Override
            public void onDismiss()
            {
                reservationGuide = null;
                if(dialog!=null)
                {
                    dialog.dismiss(); // dismiss the empty dialog when the PopupWindow closes
                    dialog = null;
                }
            }
        });
        dialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationGuide = null;
                if(dialog!=null)
                {
                    dialog.dismiss(); // dismiss the empty dialog when the PopupWindow closes
                    dialog = null;
                }
            }
        });

        btnUnderstood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reservationGuide = null;
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(SHOW_TIP_KEY, true);
                editor.commit();
                if(dialog!=null)
                {
                    dialog.dismiss(); // dismiss the empty dialog when the PopupWindow closes
                    dialog = null;
                }
            }
        });
    }

    // call this method when required to show popup
    void onShowPopup(final View v, final Reservation reservation){

        LayoutInflater layoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.popup_layout, null,false);
        // find the ListView in the popup layout
        final ListView listView = (ListView)inflatedView.findViewById(R.id.lst_actions);

        dialog = new Dialog(getContext(), android.R.style.Theme_Translucent_NoTitleBar);
        final View emptyDialog = LayoutInflater.from(getContext()).inflate(R.layout.empty, null);


        // fill the data to the list items
        final ArrayList<popUpAction> actions = getReservationMoreActions();

        listView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return actions.size();
            }

            @Override
            public Object getItem(int position) {
                return actions.get(position);
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if(convertView == null){
                    LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.popup_action_layout, parent, false);
                }

                popUpAction action = (popUpAction) getItem(position);

                ((ImageView)convertView.findViewById(R.id.imgIcon)).setImageResource(action.iconResID);
                ((TextView)convertView.findViewById(R.id.txtTitle)).setText(action.title);
                ((TextView)convertView.findViewById(R.id.txtSubtitle)).setText(action.subtitle);

                return convertView;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popWindow.dismiss();

                popUpAction action = (popUpAction) listView.getAdapter().getItem(position);
                if(action instanceof showRoomPopUpAction) {
                    Intent intent = new Intent(getContext(), RoomActivity.class);
                    intent.putExtra("id", reservation.room);
                    intent.putExtra("tab", "info");
                    getContext().startActivity(intent);
                } else if(action instanceof deleteReservationPopUpAction){
                    ReservationsManager.getInstance().deleteReservation(reservation);
                } else if (action instanceof reminderPopUpAction){
                    if(notificationManagerListener != null) {
                        notificationManagerListener.onNotificationManagerAsked();
                    }
                }
            }
        });

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        popWindow.setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);

        // make it outside touchable to dismiss the popup window
        popWindow.setOutsideTouchable(true);

         /* blur background*/
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.dimAmount=0.6f;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        dialog.setContentView(emptyDialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setOnShowListener(new DialogInterface.OnShowListener()
        {
            @Override
            public void onShow(DialogInterface dialogIx)
            {
                // show the popup at bottom of the screen and set some margin at bottom ie,
                popWindow.showAtLocation(emptyDialog, Gravity.BOTTOM, 0, 0);
            }
        });
        popWindow.setOnDismissListener(new PopupWindow.OnDismissListener()
        {
            @Override
            public void onDismiss()
            {
                if(dialog!=null)
                {
                    dialog.dismiss(); // dismiss the empty dialog when the PopupWindow closes
                    dialog = null;
                }
            }
        });
        dialog.show();
    }


    /**
     * Get the list of actions available on a specific reservation
     * @return list of actions available on a specific reservation
     */
    ArrayList<popUpAction> getReservationMoreActions(){
        ArrayList<popUpAction> actions = new ArrayList<popUpAction>();

        popUpAction showRoomAction = new showRoomPopUpAction();
        showRoomAction.popUpActionClicked = new IPopUpActionClicked() {
            @Override
            public void onPopUpActionClicked() {

            }
        };


        popUpAction reminder = new reminderPopUpAction();
        reminder.popUpActionClicked = new IPopUpActionClicked() {
            @Override
            public void onPopUpActionClicked() {

            }
        };


        popUpAction delete = new deleteReservationPopUpAction();
        delete.popUpActionClicked = new IPopUpActionClicked() {
            @Override
            public void onPopUpActionClicked() {

            }
        };

        actions.add(showRoomAction);
        actions.add(reminder);
        actions.add(delete);

        return actions;
    }


    /**
     * Pop up view action button
     */
    class popUpAction {
        public int iconResID;
        public String title;
        public String subtitle;
        public IPopUpActionClicked popUpActionClicked;
    }

    /**
     * Pop up view action button: configured to show the reservation room information
     */
    class showRoomPopUpAction extends popUpAction{
        public showRoomPopUpAction(){
            this.iconResID = R.drawable.about;
            this.title = getResources().getString(R.string.showRoomTitle);
            this.subtitle = getResources().getString(R.string.showRoomDesc);
        }
    }

    /**
     * Pop up view action button: configured to delete reservation
     */
    class deleteReservationPopUpAction extends popUpAction{
        public deleteReservationPopUpAction(){
            this.iconResID = R.drawable.delete;
            this.title = getResources().getString(R.string.deleteTitle);
            this.subtitle = getResources().getString(R.string.deleteDesc);
        }
    }

    /**
     * Pop up view action button: configured to manage notifications
     */
    class reminderPopUpAction extends popUpAction{
        public reminderPopUpAction(){
            this.iconResID = R.drawable.alarm_clock;
            this.title = getResources().getString(R.string.reminderTitle);
            this.subtitle = getResources().getString(R.string.reminderDesc);
        }
    }

    interface IPopUpActionClicked {
        public void onPopUpActionClicked();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //pour cacher les bouttons des fragments
        MenuItem item1=menu.findItem(R.id.action_filter);
        MenuItem item2=menu.findItem(R.id.action_calendar);
        getActivity().setTitle(R.string.my_reservations);
        item1.setVisible(false);
        item2.setVisible(false);
    }

    interface INotificationManagerListener {
        public void onNotificationManagerAsked();
        public void onReservationDeleted(String reservationId);
    }
}
