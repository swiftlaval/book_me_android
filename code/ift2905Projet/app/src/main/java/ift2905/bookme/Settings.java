package ift2905.bookme;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import core.ift2950.bookme.ErrorType;
import core.ift2950.bookme.SettingsManager;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends Fragment {

    final int NOTIFICATION_BEFORE_TIME_UNIT = 15;
    core.ift2950.bookme.Settings settings = null;

    public Settings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);//ne pas effacer

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        final TextView txtNotificationTime = (TextView)view.findViewById(R.id.txtNotificationTime);

        final TextView txtNotificationDesc = (TextView)view.findViewById(R.id.txtNotificationDesc);

        final SeekBar skNotificationTime = (SeekBar)view.findViewById(R.id.skNotificationTime);

        final Switch notificationSwitch = (Switch)view.findViewById(R.id.swActivateNotification);
        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                skNotificationTime.setEnabled(isChecked);
                txtNotificationTime.setEnabled(isChecked);
                txtNotificationDesc.setEnabled(isChecked);

                Boolean success = SettingsManager.getInstance().updateCurrentUserSettings(notificationSwitch.isChecked(), skNotificationTime.getProgress());
            }
        });

        skNotificationTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtNotificationTime.setText(getNotificationTime(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Boolean success = SettingsManager.getInstance().updateCurrentUserSettings(notificationSwitch.isChecked(), skNotificationTime.getProgress());
                if(!success){

                }
            }
        });

        SettingsManager.getInstance().syncUserSettings(FirebaseAuth.getInstance().getCurrentUser().getEmail(), new SettingsManager.ISettingsListener() {
            @Override
            public void onSettingsUpdated(core.ift2950.bookme.Settings newSettings) {
                settings = newSettings;
                txtNotificationTime.setText(getNotificationTime(settings.notificationTime));
                notificationSwitch.setChecked(settings.notificationActivated);
                skNotificationTime.setProgress(settings.notificationTime);
            }

            @Override
            public void onError(ErrorType errorType) {

            }
        });


        return view;
    }


    String getNotificationTime(int timeUnit){
        return (timeUnit * NOTIFICATION_BEFORE_TIME_UNIT) + " min";
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        //pour cacher les bouttons des fragments
        MenuItem item1=menu.findItem(R.id.action_filter);
        MenuItem item2=menu.findItem(R.id.action_calendar);
        getActivity().setTitle(R.string.settings);
        item1.setVisible(false);
        item2.setVisible(false);
    }
}
