package ift2905.bookme;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.firebase.auth.FirebaseAuth;

import core.ift2950.bookme.*;
import core.ift2950.bookme.Settings;

/**
 * Created by ekabore on 17-04-30.
 */
public class NotificationPublisher extends BroadcastReceiver {

    public static String NOTIFICATION_IDENTIFIER = "notificationIdentifier";
    public static String NOTIFICATION = "notification";

    public void onReceive(final Context context, final Intent intent) {
        NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

        Notification notification = intent.getParcelableExtra(NOTIFICATION);
        int id = intent.getIntExtra(NOTIFICATION_IDENTIFIER, 0);
        notificationManager.notify(id, notification);
    }
}
