package core.ift2950.bookme;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by ekabore on 17-04-05.
 *
 * Searches for rooms available. Takes a filter of rooms properties and return matching rooms
 *
 * To get rooms that match a specific filter, first set that filter using "setFilters" then,
 * retrieve result using "getRooms"
 *
 */
public class RoomsSearchEngine {

    /**
     * Singleton for the search engine
     */
    static RoomsSearchEngine roomsSearchEngine = new RoomsSearchEngine();

    public static RoomsSearchEngine getInstance(){
        return roomsSearchEngine;
    }




    private DatabaseReference mDatabaseRoomsReference;

    private ArrayList<RoomManager> roomsFound;                        //All rooms availables
    private ArrayList<RoomManager> filteredRoomsList;                 //Filtered list of rooms available according filters option

    //Rooms filters option
    private Integer currentCapacityFilter;
    private Integer currentCapacity2Filter;
    private Integer currentCapacity3Filter;
    private Integer currentCapacity8Filter;
    private Integer currentHasScreenFilter;
    private Integer currentHasBlackboardFilter;
    private Integer currentHasPrinterFilter;

    private boolean hasFiltersChanged;                                 //Indicates if filteredRoomList must be reloaded


    /**
    * Set filters on rooms available. To retreive matching rooms use "getRooms" method
    *
    * For each filter value: -1 indicates that the filter value does not matter
    *
    * @param  capacity          filter on room size (number of people)
    * @param  withBlackboard    rooms with blackboards ?
    * @param  withBlackboard    rooms with blackboards ? 0 = false; anything else expect -1 = true
    * @param  withPrinter       rooms with printer ? 0 = false; anything else expect -1 = true
    * @param  withScreen       rooms with Screen ? 0 = false; anything else expect -1 = true
    */
    public void setFilters(Integer capacity,Integer capacity2,Integer capacity3,Integer capacity8, Integer withBlackboard, Integer withPrinter, Integer withScreen){

        hasFiltersChanged = (this.currentCapacity2Filter != capacity2)||
                (this.currentCapacity3Filter != capacity3)||
                (this.currentCapacity8Filter != capacity8) ||
                (this.currentCapacityFilter != capacity) ||
                (this.currentHasScreenFilter != withScreen)||
                (this.currentHasBlackboardFilter != withBlackboard) ||
                (this.currentHasPrinterFilter != withPrinter);

        this.currentCapacityFilter = capacity;
        this.currentCapacity2Filter = capacity2;
        this.currentCapacity3Filter = capacity3;
        this.currentCapacity8Filter = capacity8;
        this.currentHasScreenFilter = withScreen;
        this.currentHasBlackboardFilter = withBlackboard;
        this.currentHasPrinterFilter = withPrinter;
    }


    /**
     * Gets the room manager for the room with id roomId.
     * Call this function only after you call first getRooms to initialise the room list
     *
     * @param roomId the id of the room
     * @return
     */
    public RoomManager getRoomManager(String roomId){
        RoomManager roomManager = null;
        for (RoomManager room : roomsFound) {
            if(room.getRoom().id.equals(roomId)){
                roomManager = room;
                break;
            }
        }
        return roomManager;
    }


    /**
    * Retrieves Rooms for a specific date. The rooms returned will be filtered by the current set of filters.
    * To change filters see "setFilters" method
    *
    *
    * @param date room agenda date
    * @param roomsListener listener for matching room
    */
    public void getRooms(final Date date, final IFoundRoomsListener roomsListener) {
        if(roomsFound.size() == 0){
            //Try fetch or refresh data from database
            this.mDatabaseRoomsReference.orderByKey().addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot roomSnapshot: dataSnapshot.getChildren()) {
                        Room room = roomSnapshot.getValue(Room.class);
                        room.id = roomSnapshot.getKey();
                        RoomManager manager = new RoomManager(room, date);

                        roomsFound.add(manager);

                    }

                    hasFiltersChanged = false;
                    filteredRoomsList = applyFilter(roomsFound);

                    if(roomsListener != null){
                        roomsListener.onRoomsFound(filteredRoomsList);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    roomsListener.onError(Utilities.getErrorType(databaseError));
                }
            });
        } else {
            //Rooms are already fetched from database, adjust the rooms agenda date accordingly
            Runnable filterTask = new Runnable() {
                @Override
                public void run() {
                    for (RoomManager manager : roomsFound) {
                        manager.setAgendaDate(date);
                    }

                    if(hasFiltersChanged) {
                        filteredRoomsList = applyFilter(roomsFound);
                        hasFiltersChanged = false;
                    }

                    if(roomsListener != null){
                        roomsListener.onRoomsFound(filteredRoomsList);
                    }
                }
            };

            Thread filterAndNotificationThread = new Thread(filterTask);
            filterAndNotificationThread.start();
        }
    }


    /**
     *  Applies current filters on a list of room managers and return the filtered list
     *  @param roomManagerList the room managers list to filter according the current filters
     *  @return the filtered list
     */
    private ArrayList<RoomManager> applyFilter(ArrayList<RoomManager> roomManagerList){

        ArrayList<RoomManager> filteredResult = new ArrayList<RoomManager>();

        for (RoomManager manager : roomManagerList) {
            Room room = manager.getRoom();

            /*
            * Verifies for each filter if it must be used in the computation then apply it or not
            * -1 tells that the filter must not be taken in consideration.
            * 0 stands as false for boolean filters and other values except -1 as true
            */

            if(this.currentCapacityFilter != -1 && room.capacity != this.currentCapacityFilter){
               continue;
            }

            if(this.currentCapacity2Filter != -1 && room.capacity != 8 && room.capacity != 3){
                continue;
            }

            if(this.currentCapacity3Filter != -1 && room.capacity != 2 && room.capacity != 8){
                continue;
            }

            if(this.currentCapacity8Filter != -1 && room.capacity != 2 && room.capacity != 3){
                continue;
            }

            if(this.currentHasBlackboardFilter != -1 && room.hasBlackboard != (this.currentHasBlackboardFilter != 0)){
                continue;
            }

            if(this.currentHasScreenFilter != -1 && room.hasScreen != (this.currentHasScreenFilter != 0)){
                continue;
            }

            if(this.currentHasPrinterFilter != -1 && room.hasPrinter != (this.currentHasPrinterFilter != 0)){
                continue;
            }


            filteredResult.add(manager);
        }

        return filteredResult;
    }


    /**
     * Constructor
     */
    public RoomsSearchEngine() {
        this.mDatabaseRoomsReference = FirebaseDatabase.getInstance().getReference().child("rooms");
        this.roomsFound = new ArrayList<RoomManager>();
        this.filteredRoomsList = new ArrayList<RoomManager>();
        this.currentCapacityFilter = -1;
        this.currentCapacity2Filter = -1;
        this.currentCapacity3Filter = -1;
        this.currentCapacity8Filter = -1;
        this.currentHasScreenFilter = -1;
        this.currentHasBlackboardFilter = -1;
        this.currentHasPrinterFilter = -1;
    }


    /**
     * Provides an interface to collect the list of Rooms managers found or available.
     * Also used to communicate error if one occurs while building the room managers list.
     */
    public interface IFoundRoomsListener {
        /*
        * Triggered when some rooms are available after a search
        */
        public void onRoomsFound(List<RoomManager> roomManagers);

        /*
        * Triggered when an error occurs while fetching rooms available
        */
        public void onError(ErrorType errorType);
    }
}
