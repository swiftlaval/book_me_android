package core.ift2950.bookme;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ekabore on 17-04-07.
 *
 * Manages a specific user reservations
 *
 */
public class ReservationsManager {

    private DatabaseReference mDatabaseReservationsReference;
    private Query mDatabaseUserReservationsQuery;
    private ChildEventListener userReservationQueryListener;
    private ValueEventListener reservationQueryListener;
    private IReservationsListener currentReservationListener;       //Listens for user reservations changes (Add, deletion)

    private String user = "";
    private HashMap<String, Reservation> reservationsFound;        //Map<ReservationID, Reservation> user reservations
    private Boolean isReservationEmpty = false;


    static ReservationsManager reservationsManager = new ReservationsManager();
    public static ReservationsManager getInstance(){
        return reservationsManager;
    }


    /**
     * Reserves a room for the user
     * @param userId   user id
     * @param roomId   room id
     * @param date     reservation date
     * @param description   short description for the reservation
     * @param startHour     reservation beginning hour
     * @param duration      reservation duration
     * @return the id of the reservation
     */
    public String addReservation(String userId, String roomId, Date date, String description, Integer startHour, Integer duration){
        String dateId = Utilities.getFirebaseDateIdFromDate(date);
        String reservationId = dateId + "-" + String.valueOf(startHour) + "-" + roomId;

        Reservation reservationToAdd = new Reservation();
        reservationToAdd.date = dateId;
        reservationToAdd.description = description == null ? "" : description;
        reservationToAdd.room = roomId;
        reservationToAdd.user = userId;
        reservationToAdd.timeSlot = new HashMap<String, Integer>();
        reservationToAdd.timeSlot.put(String.valueOf(startHour), duration);

        //Set in occupations node, reservation for the specified room
        for (int i = 0; i < duration; i++){
            FirebaseDatabase.getInstance().getReference().child("occupations")
                    .child(dateId).child(roomId).child(String.valueOf(startHour + i))
                    .setValue(reservationId);
        }

        //Create reservation itself
        mDatabaseReservationsReference.child(reservationId).setValue(reservationToAdd);

        return reservationId;
    }


    /**
     * Delete a reservation
     * @param reservation the reservation to delete
     */
    public void deleteReservation(Reservation reservation){
        if(reservationsFound.values().contains(reservation)){
            //Delete room occupations that belong to that reservation
            for (String startHour : reservation.timeSlot.keySet()) {
                Integer duration = reservation.timeSlot.get(startHour);
                for (int i = 0; i < duration; i++){
                    FirebaseDatabase.getInstance().getReference().child("occupations")
                            .child(reservation.date).child(reservation.room)
                            .child(String.valueOf(Integer.valueOf(startHour) + i)).removeValue();
                }
            }

            //Delete reservation itself
            mDatabaseReservationsReference.child(reservation.id).removeValue();
        }
    }


    /**
     * Synchronize user reservations
     * @param user
     * @param reservationsListener
     */
    public void syncUserReservation(String user, IReservationsListener reservationsListener){
        if(user == null || user.equals("")){

            if(mDatabaseUserReservationsQuery != null) {
                mDatabaseUserReservationsQuery.removeEventListener(userReservationQueryListener);
            }

            mDatabaseReservationsReference.removeEventListener(reservationQueryListener);

            this.currentReservationListener = null;

            return;
        }

        this.currentReservationListener = reservationsListener;
        if(this.user == null || !this.user.equals(user)){
            this.reservationsFound.clear();
            this.user = user;

            notifyStartOfSynchronization();

            if(mDatabaseUserReservationsQuery != null)
                mDatabaseUserReservationsQuery.removeEventListener(userReservationQueryListener);

            mDatabaseReservationsReference.removeEventListener(reservationQueryListener);

            mDatabaseReservationsReference.addValueEventListener(reservationQueryListener);

        } else {
            notifyStartOfSynchronization();
        }
    }

    /**
     * Notifies that the synchronization has begun
     */
    void listenForUserReservation(String user) {
        mDatabaseUserReservationsQuery = this.mDatabaseReservationsReference.orderByChild("user").equalTo(this.user);
        mDatabaseUserReservationsQuery.addChildEventListener(userReservationQueryListener);
    }


    /**
     * Notifies that the synchronization has begun
     */
    private void notifyStartOfSynchronization(){
        if(currentReservationListener != null) {
            currentReservationListener.onStartFetchingReservations(this.user, new ArrayList<Reservation>(this.reservationsFound.values()));
            if(isReservationEmpty){
                currentReservationListener.onUserReservationEmpty();
            }
        }
    }


    /**
     * Constructor
     */
    public ReservationsManager(){
        this.mDatabaseReservationsReference = FirebaseDatabase.getInstance().getReference().child("reservations");
        this.reservationsFound = new HashMap<String, Reservation>();
        this.mDatabaseUserReservationsQuery = null;
        reservationQueryListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == null){
                    isReservationEmpty = true;
                    if(currentReservationListener != null) {
                        currentReservationListener.onUserReservationEmpty();
                    }
                } else {
                    Boolean found = false;
                    for (DataSnapshot snap : dataSnapshot.getChildren()) {
                        Reservation reservation = snap.getValue(Reservation.class);
                        if(reservation.user != null && reservation.user.equals(user)){
                            found = true;
                            isReservationEmpty = false;
                            mDatabaseReservationsReference.removeEventListener(reservationQueryListener);
                            listenForUserReservation(user);
                            break;
                        }
                    }
                    if(!found){
                        isReservationEmpty = true;
                        if(currentReservationListener != null) {
                            currentReservationListener.onUserReservationEmpty();
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        this.userReservationQueryListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                isReservationEmpty = false;
                Reservation reservation = dataSnapshot.getValue(Reservation.class);
                reservation.id = dataSnapshot.getKey();
                reservationsFound.put(reservation.id, reservation);
                if(currentReservationListener != null)
                    currentReservationListener.onReservationFound(reservation);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String reservationID = dataSnapshot.getKey();
                Reservation reservationDeleted = reservationsFound.remove(reservationID);
                if(reservationDeleted != null && currentReservationListener != null){
                    currentReservationListener.onReservationDeleted(reservationDeleted);
                }

                if(reservationsFound.size() == 0) {
                    isReservationEmpty = true;
                    if (currentReservationListener != null) {
                        currentReservationListener.onUserReservationEmpty();
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(currentReservationListener != null)
                    currentReservationListener.onError(Utilities.getErrorType(databaseError));
            }
        };
    }


    /**
     * Listens for user reservations events (Deletion, synchronization operations)
     */
    public interface IReservationsListener{
        public void onReservationFound(Reservation reservation);
        public void onReservationDeleted(Reservation reservation);
        public void onStartFetchingReservations(String user, List<Reservation> allReadyFetched);
        public void onUserReservationEmpty();
        public void onError(ErrorType errorType);
    }
}
