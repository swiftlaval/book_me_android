package core.ift2950.bookme;

import com.google.firebase.database.DatabaseError;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by ekabore on 17-04-11.
 *
 *
 *
 */
class Utilities {

    /**
     * Converts java Date to internal system custom date identifier
     * @param date
     * @return custom date identifier
     */
    public static String getFirebaseDateIdFromDate(Date date){
        if(date == null) return "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        int monthValue = calendar.get(Calendar.MONTH) + 1;
        String month = monthValue < 10 ? "0" + String.valueOf(monthValue) : String.valueOf(monthValue);
        int dayValue = calendar.get(Calendar.DAY_OF_MONTH);
        String day = dayValue < 10 ? "0" + String.valueOf(dayValue) : String.valueOf(dayValue);

        return year + month + day;
    }


    /**
     * Gets the error type of the specified firebase database error
     * @param error firebase database error
     * @return the error type associated
     */
    public static ErrorType getErrorType(DatabaseError error){
        ErrorType errorType = ErrorType.INTERNAL_SERVER_ERROR;

        switch (error.getCode()){
            case -4:
                errorType = ErrorType.DISCONNECTED;
                break;
            case -24:
                errorType = ErrorType.NETWORK_ERROR;
                break;
            case -10:
                errorType = ErrorType.SERVICE_UNAVAILABLE;
                break;
        }


        return errorType;
    }
}
