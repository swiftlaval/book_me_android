package core.ift2950.bookme;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ekabore on 17-03-30.
 *
 *
 * Room representation
 *
 *
 */
@IgnoreExtraProperties
public class Room {
    @Exclude
    public String id;

    public int capacity;
    public boolean hasBlackboard;
    public boolean hasPrinter;
    public boolean hasScreen;
    public String imageUrl;

    public Room() {

    }
}
