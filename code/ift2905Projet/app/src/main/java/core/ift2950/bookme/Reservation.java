package core.ift2950.bookme;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ekabore on 17-04-05.
 *
 * Reservation representation
 *
 *
 */
@IgnoreExtraProperties
public class Reservation {
    @Exclude
    public String id;

    public String date;
    public String description;
    public String room;
    public Map<String, Integer> timeSlot = new HashMap<String, Integer>();
    public String user;


    @Exclude
    public Date getDate(){
        Date dateToReturn = null;

        if(date != null && !date.equals("")){
            DateFormat format = new SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
            try {
                dateToReturn = format.parse(date);
            } catch (ParseException e) {
            }
        }

        return dateToReturn;
    }

    public Reservation(){

    }
}
