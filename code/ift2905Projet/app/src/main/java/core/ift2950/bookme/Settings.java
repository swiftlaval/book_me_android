package core.ift2950.bookme;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by ekabore on 17-04-25.
 *
 * A user settings
 */
@IgnoreExtraProperties
public class Settings {
    public boolean notificationActivated;
    public int notificationTime;
    public String user;

    public Settings() {

    }
}
