package core.ift2950.bookme;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Created by ekabore on 17-04-25.
 */

public class SettingsManager {

    String currentUser;
    ISettingsListener currentSettingsListener;

    DatabaseReference mDatabaseSettingsReference;
    Query mDatabaseUserSettingsQuery;
    ChildEventListener referenceChildEventListener;

    Settings currentUserSettings;
    String currentSettingID = "";


    static SettingsManager manager = new SettingsManager();
    public static SettingsManager getInstance (){
        return manager;
    }


    public void syncUserSettings(String user, ISettingsListener settingsListener){
        if(user == null || user.equals("")){
            if(mDatabaseUserSettingsQuery != null) {
                mDatabaseUserSettingsQuery.removeEventListener(referenceChildEventListener);
            }

            this.currentSettingsListener = null;

            return;
        }

        this.currentSettingsListener = settingsListener;
        if(this.currentUser == null || !this.currentUser.equals(user)){
            this.currentUser = user;

            if(mDatabaseUserSettingsQuery != null){
                mDatabaseUserSettingsQuery.removeEventListener(referenceChildEventListener);
            }

            mDatabaseUserSettingsQuery = mDatabaseSettingsReference.orderByChild("user").equalTo(this.currentUser);

            mDatabaseUserSettingsQuery.addChildEventListener(referenceChildEventListener);

        } else {
            if(this.currentSettingsListener != null){
                this.currentSettingsListener.onSettingsUpdated(this.currentUserSettings);
            }
        }
    }

    public Boolean updateCurrentUserSettings(Boolean notificationActivated, Integer notificationTime){
        if(currentSettingID != ""){
            currentUserSettings.notificationActivated = notificationActivated;
            currentUserSettings.notificationTime = notificationTime;
            mDatabaseSettingsReference.child(currentSettingID).setValue(currentUserSettings);
            return true;
        } else {
            return false;
        }
    }

    public SettingsManager() {
        this.mDatabaseSettingsReference = FirebaseDatabase.getInstance().getReference().child("settings");

        this.referenceChildEventListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                currentSettingID = dataSnapshot.getKey();
                currentUserSettings = dataSnapshot.getValue(Settings.class);
                if(currentSettingsListener != null){
                    currentSettingsListener.onSettingsUpdated(currentUserSettings);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                currentSettingID = dataSnapshot.getKey();
                currentUserSettings = dataSnapshot.getValue(Settings.class);
                if(currentSettingsListener != null){
                    currentSettingsListener.onSettingsUpdated(currentUserSettings);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(currentSettingsListener != null)
                    currentSettingsListener.onError(Utilities.getErrorType(databaseError));
            }
        };
    }

    /**
     * Listens for user settings events (update, synchronization operations)
     */
    public interface ISettingsListener{
        public void onSettingsUpdated(Settings settings);
        public void onError(ErrorType errorType);
    }
}

