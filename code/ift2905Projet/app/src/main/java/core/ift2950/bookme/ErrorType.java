package core.ift2950.bookme;

/**
 * Created by ekabore on 17-04-11.
 *
 * Kind of error returned if some occurs while communicating with firebase
 *
 *
 */
public enum ErrorType {
    /**
     * Operation aborted due to network disconnection
     */
    DISCONNECTED,

    /**
     * Operation cannot be performed due to network error
     */
    NETWORK_ERROR,

    /**
     * Firebase service is not available
     */
    SERVICE_UNAVAILABLE,

    /**
     * Operation failed due to firebase internal system error
     */
    INTERNAL_SERVER_ERROR
}
