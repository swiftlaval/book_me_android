package core.ift2950.bookme;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by ekabore on 17-04-05.
 *
 * Manages the agenda of a specific room
 *
 */
public class RoomManager {

    //Firebase delete entire node when the node inner value becomes null,
    // so to prevent Firebase from deleting node, we add a fake Hour. Keeping node helps improves future search request
    private static final String FAKE_HOUR = "preventNodeDelete";

    private Date currentAgendaDate;

    private Room room;

    private DatabaseReference mDatabaseOccupationsReference;            //reference to occupations node
    private ChildEventListener occupationListener;

    private DatabaseReference mDatabaseOccupationOnCurrentDate;         //reference to occupations/$date node
    private ChildEventListener dateOccupationListener;

    private DatabaseReference mDatabaseRoomOccupation;                  //reference to occupations/$date/$room node
    private ChildEventListener roomOccupationListener;


    private HashMap<String, String> occupations;                        //Map<Hour, reservationID>

    private IRoomAvailabilityChanged roomAvailabilityChangedListener;  //listener for room agenda changes


    /**
     * Sets the listener for room agenda changes
     * @param listener listens for room agenda changes
     */
    public void setRoomAvailabilityChangedListener(IRoomAvailabilityChanged listener){
        this.roomAvailabilityChangedListener = listener;

        if(roomAvailabilityChangedListener != null) {
            for (String hour : occupations.keySet()) {
                roomAvailabilityChangedListener.onRoomAvailabilityChanged(hour, true);
            }
        }
    }


    /**
     * Gets the rooms managed by this manager
     * @return the room associated to that manager
     */
    public Room getRoom(){
        return this.room;
    }


    /**
     * Sets the room current aganda date. Agenda changes will be reported to the room availability chnaged listener
     * @param agendaDate date for which we need to know the room availabilities
     */
    public void setAgendaDate(Date agendaDate){
        String dateId = Utilities.getFirebaseDateIdFromDate(agendaDate);
        String currentAgendaDateId = Utilities.getFirebaseDateIdFromDate(currentAgendaDate);
        if(!currentAgendaDateId.equals(dateId)){
            this.occupations.clear();

            if(occupationListener != null){
                mDatabaseOccupationsReference.removeEventListener(occupationListener);
            }

            if(mDatabaseOccupationOnCurrentDate != null && dateOccupationListener != null){
                mDatabaseOccupationOnCurrentDate.removeEventListener(dateOccupationListener);
            }

            if(mDatabaseRoomOccupation != null && roomOccupationListener != null){
                mDatabaseRoomOccupation.removeEventListener(roomOccupationListener);
            }

            listenOnOccupationsForEvents(dateId);

            this.currentAgendaDate = agendaDate;
        }
    }


    /**
     * Listen on occupations node for changes on the date in parameters
     * @param date date to listen for occupations changes
     */
    private void listenOnOccupationsForEvents(final String date) {
        this.occupationListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals(date)){
                    mDatabaseOccupationOnCurrentDate = mDatabaseOccupationsReference.child(date);
                    listenOnDateOccupationsForEvents(room.id);

                    mDatabaseOccupationsReference.removeEventListener(occupationListener);
                    occupationListener = null;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(roomAvailabilityChangedListener != null) {
                    roomAvailabilityChangedListener.onError(Utilities.getErrorType(databaseError));
                }
            }
        };

        this.mDatabaseOccupationsReference.addChildEventListener(occupationListener);
    }


    /**
     * Listen on occupations->date node for changes on room having the id specified in parameters
     * @param roomId room to listen for occupations changes
     */
    private void listenOnDateOccupationsForEvents(final String roomId) {
        this.dateOccupationListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getKey().equals(roomId)){
                    mDatabaseRoomOccupation = mDatabaseOccupationOnCurrentDate.child(room.id);
                    listOnRoomOccupationsForEvents();

                    mDatabaseOccupationOnCurrentDate.removeEventListener(dateOccupationListener);
                    dateOccupationListener = null;
                    mDatabaseOccupationOnCurrentDate = null;
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(roomAvailabilityChangedListener != null) {
                    roomAvailabilityChangedListener.onError(Utilities.getErrorType(databaseError));
                }
            }
        };

        this.mDatabaseOccupationOnCurrentDate.addChildEventListener(dateOccupationListener);
    }


    /**
     * Listen for changes at a specific room node
     */
    private void listOnRoomOccupationsForEvents(){
        this.roomOccupationListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                String hour = dataSnapshot.getKey();
                String reservationID = dataSnapshot.getValue(String.class);
                occupations.put(hour, reservationID);
                if (hour != FAKE_HOUR && roomAvailabilityChangedListener != null) {
                    roomAvailabilityChangedListener.onRoomAvailabilityChanged(hour, true);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                String hour = dataSnapshot.getKey();
                occupations.remove(hour);
                if(roomAvailabilityChangedListener != null) {
                    roomAvailabilityChangedListener.onRoomAvailabilityChanged(hour, false);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if(roomAvailabilityChangedListener != null) {
                    roomAvailabilityChangedListener.onError(Utilities.getErrorType(databaseError));
                }
            }
        };

        this.mDatabaseRoomOccupation.addChildEventListener(roomOccupationListener);
    }


    /**
     * Gets the current agenda date
     * @return current agenda date
     */
    public Date getAgendaDate() {
        return this.currentAgendaDate;
    }


    /**
     * Check whether the room is available at the specified hour
     * @param hour agenda hour : 8 to 10
     * @return the availability of the room
     */
    public boolean IsAvailable(Integer hour){
        return this.occupations.containsKey(String.valueOf(hour));
    }


    /**
     * Constructor
     * @param room              room to be manage
     * @param agendaDate        initial agenda date
     */
    public RoomManager(Room room, Date agendaDate){
        this.room = room;
        this.occupations = new HashMap<String, String>();
        this.mDatabaseOccupationsReference = FirebaseDatabase.getInstance().getReference().child("occupations");
        setAgendaDate(agendaDate);
    }


    /**
     *
     */
    public interface IRoomAvailabilityChanged{
        /**
         * Notifies a change of the availability of the managed room for a specific hour
         * @param hour            hour for which the room availability changed
         * @param isAvailable    is the room still available ?
         */
        public void onRoomAvailabilityChanged(String hour, boolean isAvailable);

        /**
         * Notifies that an error occurred while fetching room availability from database
         */
        public void onError(ErrorType errorType);
    }
}
